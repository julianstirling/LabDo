LabDo
===========================

# !Warning this is very new and in a lot of flux. Do not use for code that needs to be stable!!

# What is LabDo

LabDo is an attempt to build a simple to use framework for building scientific applications in Python using on top of PyQt5. LabDo will use UI files from QtDesigner and will handle most of the GUI code and the threading, allowing the user to simply implement their experiment without worrying about software engineering.
