 
__author__ = 'Julian Stirling'

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))


setup(name = 'LabDo',
      version = '0.0.1',
      description = 'A simple to use framework for building scientific applications in Python built on top of PyQt5',
      long_description = 'LabDo is an attempt to build a simple to use framework for building scientific applications in Python built on top of PyQt5. LabDo will use UI files from QtDesigner and will handle most of the GUI code and the threading, allowing the user to simply implement their experiment without worrying about software engineering.',
      author = 'Julian Stirling',
      author_email = 'julian@julianstirling.co.uk',
      packages = find_packages(),
      keywords = ['scientific','framework','Instrument'],
      zip_safe = True,
      classifiers = [
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 2.7'
          ],
      )

